# -*- coding: utf-8 -*-
'''
Задание 1.4

Даны 3 переменные name age city

программа должна выводить строку:

Hello! My name is Ivan and Im 25 years old. Im from Moscow.

Напишите вывод строки минимум тремя способами
'''

name = 'Ivan'
age = '25'
city = 'Moscow'

print('Hello! My name is {} and Im {} years old. Im from {}.'.format(name,age,city))
print('Hello! My name is '+name+' and Im '+age+' years old. Im from '+city+'.')
print(f'Hello! My name is {name} and Im {age} years old. Im from {city}.')

