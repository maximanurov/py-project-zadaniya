# -*- coding: utf-8 -*-
'''
Задание 1.6

Обработать строку vova и вывести информацию на стандартный поток вывода в виде:
name:                  Владимир
ip:                    10.0.13.3
город:                 Moscow
date:                  15.03.2020
time:                  15:20

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

vova = 'O Владимир       15.03.2020 15:20 10.0.13.3, 3d18h, Moscow/5'

print('name:'.ljust(15)+vova.split()[1])
print('ip:'.ljust(15)+vova.split()[4][:-1])
print('город:'.ljust(15)+vova.split()[-1][:-2])
print('date:'.ljust(15)+vova.split()[2])
print('time:'.ljust(15)+vova.split()[3])
