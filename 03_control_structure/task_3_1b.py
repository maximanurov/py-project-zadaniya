'''
Задание 3.1b
Скопировать код из предыдущего задания


Допишите код, чтобы:
    - после вывода результата код запускался заново и опять запрашивал ввод выражения;
    - при вводе пустой строки программа останавливалась.

'''
while True:
    calc=input('Введите выражение: ')
    if calc== '':
        break
    if '+' in calc:
        x=int(calc.split('+')[0])
        y=int(calc.split('+')[1])
        print('Результат сложения: ' +str(x+y))
    elif '-' in calc:
        x=int(calc.split('-')[0])
        y=int(calc.split('-')[1])
        print('Результат вычитания: ' +str(x-y))
    elif '*' in calc:
        x=int(calc.split('*')[0])
        y=int(calc.split('*')[1])
        print('Результат умножения: ' +str(x*y))
    elif '/' in calc:
        x=int(calc.split('/')[0])
        y=int(calc.split('/')[1])
        print('Результат деления: ' +str(x/y))
    else:
        print('Невалидное выражение ')

